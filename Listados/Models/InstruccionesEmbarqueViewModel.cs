﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Listados.Models
{
    public class InstruccionesEmbarqueViewModel
    {
        public int IdInstruccion { get; set; }
        public string NroContrato { get; set; }
        public string Referencia { get; set; }
    }
}