﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Listados.Models
{
    public class AsignarContenedor
    {
        public List<AssignarContenedorInstruccionEmbarqueViewModel> Contenedores { get; set; }
    }
}