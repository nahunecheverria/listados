﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Listados.Models
{
    public class AssignarContenedorInstruccionEmbarqueViewModel
    {

        public int IdContenedor { get; set; }
        [Required]
        public string NroContenedor { get; set; }
        public string Sello { get; set; }
        [Required(ErrorMessage ="Ingresar valores correctos")]
        [Range(double.MinValue, double.MaxValue, ErrorMessage = "Ingresar solo valores numericos")]
        public decimal Tara { get; set; }
        [Required(ErrorMessage = "Ingresar valores correctos")]
        [Range(double.MinValue, double.MaxValue, ErrorMessage = "Ingresar solo valores numericos")]
        public decimal PesoMaximo { get; set; }
        public int IdInstruccion { get; set; }
        public string CreadoPor { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public int EstadoContenedor { get; set; }
    }
}