﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Listados.Models
{
    public class AsingarContenedorResponse
    {
        public string Mensaje { get; set; }
        public bool Creado { get; set; }
    }
}