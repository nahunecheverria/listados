﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Listados.Models
{
    public class CiudadViewModel
    {
        public int IdCiudad { get; set; }
        public string Ciudad1 { get; set; }
        public string Pais { get; set; }
        public bool EstadoCiudad { get; set; }
    }
}