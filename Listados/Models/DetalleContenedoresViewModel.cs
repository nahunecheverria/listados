﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Listados.Models
{
    public class DetalleContenedoresViewModel
    {
        public int IdContenedor { get; set; }
        public string NContenedor { get; set; }
        public string Sello { get; set; }
        public string CreadoPor { get; set; }
       public int IdInstruccion { get; set; }

    }
}