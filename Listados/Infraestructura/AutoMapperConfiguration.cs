﻿using Listados.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Listados.Db;
namespace Listados.Infraestructura
{
    public class AutoMapperConfiguration
    {
        public static  void Configure()
        {
            AutoMapper.Mapper.Initialize(x =>
            {
                x.CreateMap<InstruccionEmbarque, InstruccionesEmbarqueViewModel>()
                    .ForMember(d => d.IdInstruccion, o => o.MapFrom(z => z.IdInstruccion))
                    .ForMember(d => d.NroContrato, o => o.MapFrom(z => z.DetalleContrato.Contratos.CodContrato))
                    .ForMember(d => d.Referencia, o => o.MapFrom(z => z.RefInstruccion));
                x.CreateMap<Contenedores, DetalleContenedoresViewModel>()
                    .ForMember(d => d.IdContenedor, o => o.MapFrom(z => z.IdContenedor))
                    .ForMember(d => d.NContenedor, o => o.MapFrom(z => z.NContenedor))
                    .ForMember(d => d.Sello, o => o.MapFrom(z => z.Sello))
                    .ForMember(d => d.CreadoPor, o => o.MapFrom(z => z.CreadoPor))
                    .ForMember(d => d.IdInstruccion, o => o.MapFrom(z => z.IdInstruccion));
                x.CreateMap<Ciudad, CiudadViewModel>()
                   .ForMember(d => d.IdCiudad, o => o.MapFrom(z => z.IdCiudad))
                   .ForMember(d => d.Ciudad1, o => o.MapFrom(z => z.Ciudad1))
                   .ForMember(d => d.Pais, o => o.MapFrom(z => z.Paises.Pais))
                   .ForMember(d => d.EstadoCiudad, o => o.MapFrom(z => z.EstadoCiudad));
                x.CreateMap<AssignarContenedorInstruccionEmbarqueViewModel, Contenedores>()
                .ForMember(d => d.NContenedor, o => o.MapFrom(z => z.NroContenedor))
                .ForMember(d => d.Sello, o => o.MapFrom(z => z.Sello))
                .ForMember(d => d.Tara, o => o.MapFrom(z => z.Tara))
                .ForMember(d => d.PesoMaximo, o => o.MapFrom(z => z.PesoMaximo))
                .ForMember(d => d.CreadoPor, o => o.MapFrom(z => "NEcheverria"))
                .ForMember(d => d.ModificadoPor, o => o.MapFrom(z => "NEcheverria"))
                .ForMember(d => d.FechaCreacion, o => o.MapFrom(z => DateTime.Now))
                .ForMember(d => d.FechaModificacion, o => o.MapFrom(z => DateTime.Now))
                .ForMember(d => d.EstadoContenedor, o => o.MapFrom(z => (1)))
                .ForMember(d => d.IdInstruccion, o => o.MapFrom(z => z.IdInstruccion)); 
                
            });
        }
    }
}