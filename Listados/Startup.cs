﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Listados.Startup))]
namespace Listados
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
