﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Listados.Db;
using Listados.Models;

namespace Listados.Controllers
{
    public class InstruccionController : Controller
    {
        //private ExportacionesHCFEntities db = new ExportacionesHCFEntities();


        // GET: Instruccion
        public ActionResult Index()
        {
            using (var contextxportaciones = new ExportacionesEntities())
            {
                var listaDeInstruccionesDeEmbarque = contextxportaciones.InstruccionEmbarque.Select(AutoMapper.Mapper.Map<InstruccionesEmbarqueViewModel>).ToList();
                return View(listaDeInstruccionesDeEmbarque);
            }
        }

        public ActionResult AddRowContenedor(int count)
        {
            ViewBag.Count = count;
            return PartialView();
        }

        [HttpGet]
        public ActionResult AsignarContenedor(int id)
        {
            ViewBag.IdInstruccion = id;
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult AsignarContenedor(int id, AsignarContenedor model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Error = "error message";
            }
            else
            {
                model.Contenedores.ForEach(x => x.IdInstruccion = id);
                var listaDeRespuestas = model.Contenedores.Select(AsignarContenedor).ToList();
            }

            return View(model);

        }


        public AsingarContenedorResponse AsignarContenedor(AssignarContenedorInstruccionEmbarqueViewModel model)
        {
            using (var contextExportaciones = new ExportacionesEntities())
            {
          
                try
                {
                    if (!contextExportaciones.Contenedores.Any(x=>x.IdInstruccion==model.IdInstruccion && x.NContenedor==model.NroContenedor))
                    {                      
                        var contenedor = AutoMapper.Mapper.Map<Contenedores>(model);
                      contextExportaciones.Contenedores.Add(contenedor); 
                       var result = contextExportaciones.SaveChanges() > 0;
                        return new AsingarContenedorResponse { Creado = result, Mensaje = result ? "Se ha creado con exito" : "No se pudo crear" };
                    }
                    else
                    {
                        return new AsingarContenedorResponse { Creado = false, Mensaje = "No se pudo crear, porque ya existe" };
                    }
                }
                catch (Exception ex)
                {
                    return new AsingarContenedorResponse { Creado = false, Mensaje = ex.Message };
                }
            }
        }

    
        // GET: Instruccion/Create
        //public ActionResult Create()
        //{
        //    using (var contextxportaciones = new ExportacionesEntities())
        //    {
        //        ViewBag.SelectConsignee = contextxportaciones.Consignee.ToList().Select(x => new SelectListItem {Text = x.Consignee1, Value=x.IdConsignee.ToString() });
        //        ViewBag.SelectDetalleContrato = contextxportaciones.DetalleContrato.ToList().Select(x => new SelectListItem {Text = x.Contratos.CodContrato, Value=x.IdDetalleContrato.ToString() });
        //        ViewBag.SelectLineas = contextxportaciones.Lineas.ToList().Select(x => new SelectListItem {Text = x.Linea, Value=x.IdLinea.ToString() });
        //        ViewBag.SelectNavieras = contextxportaciones.Navieras.ToList().Select(x => new SelectListItem {Text = x.Naviera, Value=x.IdNaviera.ToString() });
        //        ViewBag.SelectPuertoCarga = contextxportaciones.Puertos.ToList().Select(x => new SelectListItem {Text = x.Puerto, Value=x.IdPuerto.ToString() });
        //        ViewBag.SelectPuertoDescarga = contextxportaciones.Ciudad.ToList().Select(x => new SelectListItem {Text = x.Ciudad1, Value=x.IdCiudad.ToString() });
        //        ViewBag.SelectTerminoBl = contextxportaciones.TerminoBl.ToList().Select(x => new SelectListItem {Text = x.TerminoBl1, Value=x.IdTerminoBl.ToString() });
        //        return View();
        //    }               
        //}


    }
}
