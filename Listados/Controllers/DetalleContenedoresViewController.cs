﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Listados.Db;
using Listados.Models;

namespace Listados.Controllers
{
    public class DetalleContenedoresViewController : Controller
    {
        // GET: DetalleContenedoresView
        public ActionResult Index()
        {
            using (var contextxportaciones = new ExportacionesEntities())
            {
                var listaDeContentendores = contextxportaciones.Contenedores.Select
                    (AutoMapper.Mapper.Map<DetalleContenedoresViewModel>).ToList();
                return View(listaDeContentendores);
            }
    }
        [HttpGet]
        public ActionResult IndexModal()
        {
            using (var contextxportaciones = new ExportacionesEntities())
            {
                var detalleContenedores = contextxportaciones.Contenedores.Select(AutoMapper.Mapper.Map<DetalleContenedoresViewModel>).ToList();
                return PartialView(detalleContenedores);
            }

         
        }
    }


}