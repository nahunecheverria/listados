﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Listados.Db;
using Listados.Models;

namespace Listados.Controllers
{
    public class CiudadController : Controller
    {
        // GET: Ciudad
        public ActionResult Index()
        {
            using (var contextoExportaqciones = new ExportacionesEntities())
            {
                var listaDeCiudades = contextoExportaqciones.Ciudad.Select(AutoMapper.Mapper.Map<CiudadViewModel>).ToList();
                return View(listaDeCiudades);
            }
          
        }
    }
}