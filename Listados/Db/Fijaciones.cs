//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Listados.Db
{
    using System;
    using System.Collections.Generic;
    
    public partial class Fijaciones
    {
        public int IdFijacion { get; set; }
        public int IdContrato { get; set; }
        public bool FijacionParcial { get; set; }
        public decimal PrecioFijacion { get; set; }
        public decimal PrecioNeto { get; set; }
        public int CantSacos { get; set; }
        public Nullable<bool> FijacionConfirmada { get; set; }
        public string CreadoPor { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public string ModificadoPor { get; set; }
        public System.DateTime FechaModificacion { get; set; }
        public bool EstadoFijacion { get; set; }
    
        public virtual Contratos Contratos { get; set; }
    }
}
