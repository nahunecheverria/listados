//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Listados.Db
{
    using System;
    using System.Collections.Generic;
    
    public partial class Leyenda
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Leyenda()
        {
            this.Marca = new HashSet<Marca>();
        }
    
        public int IdLeyenda { get; set; }
        public string Leyenda1 { get; set; }
        public int IdCosecha { get; set; }
        public string CreadoPor { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public string ModificadoPor { get; set; }
        public System.DateTime FechaModificacion { get; set; }
        public bool EstadoLeyenda { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Marca> Marca { get; set; }
        public virtual Cosechas Cosechas { get; set; }
    }
}
