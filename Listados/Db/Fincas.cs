//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Listados.Db
{
    using System;
    using System.Collections.Generic;
    
    public partial class Fincas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Fincas()
        {
            this.SolicitudMuestras = new HashSet<SolicitudMuestras>();
        }
    
        public int IdFinca { get; set; }
        public string NombreFinca { get; set; }
        public int IdProcedencia { get; set; }
        public int IdProductor { get; set; }
        public string CreadoPor { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public string ModificadoPor { get; set; }
        public System.DateTime FechaModificacion { get; set; }
        public bool EstadoFinca { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SolicitudMuestras> SolicitudMuestras { get; set; }
        public virtual Procedencia Procedencia { get; set; }
        public virtual Productores Productores { get; set; }
    }
}
