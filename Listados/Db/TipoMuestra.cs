//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Listados.Db
{
    using System;
    using System.Collections.Generic;
    
    public partial class TipoMuestra
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoMuestra()
        {
            this.SolicitudMuestras = new HashSet<SolicitudMuestras>();
        }
    
        public int IdTipoMuestra { get; set; }
        public string TipoMuestra1 { get; set; }
        public string CreadoPor { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public string ModificadoPor { get; set; }
        public System.DateTime FechaModificacion { get; set; }
        public bool EstadoTipoMuestra { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SolicitudMuestras> SolicitudMuestras { get; set; }
    }
}
