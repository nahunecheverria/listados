//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Listados.Db
{
    using System;
    using System.Collections.Generic;
    
    public partial class InstruccionEmbarque
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InstruccionEmbarque()
        {
            this.SolicitudMuestras = new HashSet<SolicitudMuestras>();
            this.DetalleProgramaProduccion = new HashSet<DetalleProgramaProduccion>();
            this.Contenedores = new HashSet<Contenedores>();
            this.MarcasPorInstruccion = new HashSet<MarcasPorInstruccion>();
            this.NotifyPorInstruccion = new HashSet<NotifyPorInstruccion>();
            this.SacosLotesInstruccion = new HashSet<SacosLotesInstruccion>();
        }
    
        public int IdInstruccion { get; set; }
        public string Booking { get; set; }
        public string BillLanding { get; set; }
        public string NombreBarco { get; set; }
        public Nullable<int> IdLinea { get; set; }
        public int IdNaviera { get; set; }
        public int IdDetalleContrato { get; set; }
        public int IdConsignee { get; set; }
        public int IdPuertoCarga { get; set; }
        public int IdPuertoDescarga { get; set; }
        public int IdTrasnporte { get; set; }
        public System.DateTime FechaEmbarque { get; set; }
        public int IdTerminoBl { get; set; }
        public string CreadoPor { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public string ModificadoPor { get; set; }
        public System.DateTime FechaModificacion { get; set; }
        public bool EstadoInstruccion { get; set; }
        public int CantSacos { get; set; }
        public string RefInstruccion { get; set; }
        public Nullable<bool> FumigarContenedores { get; set; }
        public bool AsignadoMarca { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SolicitudMuestras> SolicitudMuestras { get; set; }
        public virtual Consignee Consignee { get; set; }
        public virtual DetalleContrato DetalleContrato { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetalleProgramaProduccion> DetalleProgramaProduccion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Contenedores> Contenedores { get; set; }
        public virtual Lineas Lineas { get; set; }
        public virtual Navieras Navieras { get; set; }
        public virtual Puertos Puertos { get; set; }
        public virtual Ciudad Ciudad { get; set; }
        public virtual TerminoBl TerminoBl { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MarcasPorInstruccion> MarcasPorInstruccion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NotifyPorInstruccion> NotifyPorInstruccion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SacosLotesInstruccion> SacosLotesInstruccion { get; set; }
    }
}
